import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  page: string;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToList() {
    this.page = 'swing-out';
    setTimeout(() => {
      this.router.navigate(['list']);
    }, 600);
  }
}
