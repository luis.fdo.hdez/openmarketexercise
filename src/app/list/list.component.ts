import { Component, OnInit } from '@angular/core';
import { List } from '../models/list.model';
import { fade, collapse, fadeSub } from '../animations';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  animations: [fade, collapse, fadeSub]
})
export class ListComponent implements OnInit {
  list: Array<List>;
  addItem: string;
  rotateIcon: string;
  selectedItem: List;
  subItem: string;
  showSub: boolean;

  constructor() {
    this.list = [];
  }

  ngOnInit() {
  }

  addListItem() {
    if (this.addItem !== null && this.addItem !== '' && this.addItem !== undefined) {
      this.list.push(new List(this.addItem));
      this.addItem = '';
    }
  }

  displayHidden(selected) {
    if (this.selectedItem !== undefined) {
      this.list.forEach(e => {
        if (selected !== e) {
          return e.icon = false;
        }
      });
    }
    if (this.selectedItem === selected) {
      this.selectedItem.icon = !this.selectedItem.icon;
    } else {
      this.selectedItem = selected;
      this.selectedItem.icon = !this.selectedItem.icon;
      this.selectedItem.items = new Array<List>();
    }
  }

  addSub() {
    if (this.subItem !== '' && this.subItem !== undefined && this.subItem !== null) {
      // in order to persist the data inside the arrays we need a database to store this data
      this.selectedItem.items.push(new List(this.subItem));
      this.subItem = '';
    }
  }

  deleteItem(selected) {
    const index = this.list.indexOf(selected);
    this.list.splice(index, 1);
  }

  deleteSubItem(item) {
    item.icon = !item.icon;
    setTimeout(() => {
      const index = this.selectedItem.items.indexOf(item);
      this.selectedItem.items.splice(index, 1);
    }, 200);
  }
}
