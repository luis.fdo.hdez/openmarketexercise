export class List {
  title: string;
  items: Array<List>;
  icon: boolean;

  constructor(title: string) {
    this.title = title;
    this.icon = false;
  }
}

