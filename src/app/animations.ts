import { trigger, state, style, transition, animate } from '@angular/animations';

export const fade =
trigger('fade', [
  state('void', style({opacity: 0})),
  transition(':enter', [
    animate(1000)
  ]),
  transition(':leave', [
    animate(1000, style({
      transform: 'translateX(-20px)',
      opacity: 0
    }))
  ])
]);

export const fadeSub =
trigger('fadeSub', [
  state('void', style({opacity: 0})),
  transition(':enter', [
    animate(1000)
  ]),
  transition(':leave', [
    animate(1000, style({
      transform: 'translateX(-20px)',
      backgroundColor: '#5cb85c',
      opacity: 0
    }))
  ])
]);

export const collapse =
trigger('collapse', [
  transition(':enter', [
    style({transform: 'translateY(-100%)'}),
    animate('200ms ease-in', style({transform: 'translateY(0%)'}))
  ]),
  transition(':leave', [
    animate('200ms ease-in', style({transform: 'translateY(-100%)', zIndex: -100}))
  ])
]);
